from django.db import models
from django.contrib.auth.models import User
# from datetime import datetime

class Profil(models.Model):
    user        = models.OneToOneField(User,on_delete=models.CASCADE)  # La liaison OneToOne vers le modèle User
    roleUt      = models.IntegerField()

    def __str__(self):
        return "Profil de {0}".format(self.user.username)

def  get_all_user():
    return User.objects.all()

def get_user_by_mail(mail):
    return User.objects.filter(email=mail)

def getProfilWithUser(id):
    return Profil.objects.filter(user_id=id)

def get_Question_With_ID(id):
    return Question.objects.filter(id=id)

def get_QCM_with_ID(id):
    return Questionnaire.objects.filter(id=id)

class Questionnaire(models.Model):
    titreQCM        = models.CharField(max_length=100)
    dureeQCM        = models.DurationField()
    nbMaxQst        = models.IntegerField()
    dateDebQCM      = models.DateField()
    dateFinQCM      = models.DateField()
    nbEssaiQCM      = models.IntegerField()
    entrainement    = models.BooleanField()

    def __str__(self):
        return self.titreQCM

def get_all_questionnaires():
    return Questionnaire.objects.all()

def get_qcm_nom(nom):
    return Questionnaire.objects.filter(titreQCM=nom)

class Question(models.Model):
    intituleQst     = models.CharField(max_length=200)
    image           = models.ImageField()
    quest           = models.ForeignKey(Questionnaire,on_delete=models.CASCADE)


class Reponse(models.Model):
    question        = models.ForeignKey(Question,on_delete=models.CASCADE)
    intituleReponse = models.CharField(max_length=100)
    pointReponse    = models.IntegerField()
    pointMalus      = models.IntegerField()


class TypeQuestion(models.Model):
    question        = models.OneToOneField(Question,on_delete=models.CASCADE)
    nomType         = models.CharField(max_length=100)
    descType        = models.CharField(max_length=100)

class AssociationCreateurQCM(models.Model):
    qcm             = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    createur        = models.ForeignKey(User, related_name ="createur" ,on_delete=models.CASCADE)

def get_questionnaire_createur(createur):
    return AssociationCreateurQCM.objects.filter(createur=createur)

class AssociationRealisationQCM(models.Model):
    AssociationCrea = models.ForeignKey(AssociationCreateurQCM, on_delete=models.CASCADE)
    eleves          = models.ForeignKey(User, related_name="eleve" ,on_delete=models.CASCADE)


class Groupe(models.Model):
    nom_groupe      = models.CharField(max_length=100)
    createur        = models.ForeignKey(User,on_delete=models.CASCADE)
    prive           = models.BooleanField()

def get_groupe_Createur(createur):
    return Groupe.objects.filter(createur=createur)

def get_groupe(idGroupe):
    return Groupe.objects.filter(id=idGroupe)

def get_groupes():
    return Groupe.objects.all()


class AssociationGroupeMDP(models.Model):
    nom_groupe_asso = models.ForeignKey(Groupe,on_delete=models.CASCADE)
    mdp             = models.CharField(max_length=50,null=True, blank=True)

def get_association_groupe_mdp(nomG):
    return AssociationGroupeMDP.objects.filter(nom_groupe_asso=nomG)

class AssociationGroupeEleve(models.Model):
    nom_groupe     = models.ForeignKey(Groupe,on_delete=models.CASCADE)
    eleve_groupe   = models.ForeignKey(User, on_delete=models.CASCADE)

def get_association_groupe_eleve(nomG):
    return AssociationGroupeEleve.objects.filter(nom_groupe=nomG)

def suppress_association_groupe_QCM(idQCM,infoGroupe):
    qcm = get_QCM_with_ID(idQCM)[0]
    res = AssociationGroupeQuestionnaire.objects.filter(groupe=infoGroupe).filter(questionnaire_groupe=qcm)
    res.delete()

def suppress_association_groupe_eleve(idE,groupe):
    eleve = User.objects.filter(id=idE)[0]
    res = AssociationGroupeEleve.objects.filter(eleve_groupe=eleve).filter(nom_groupe=groupe)
    res.delete()


def get_groupe_lettre(rec):
    if rec==' ':
        return get_groupes()
    cc = Groupe.objects.filter(nom_groupe__startswith=rec)
    print(cc)
    c=[]
    for row in cc:
        c.append(row)
    return c

class AssociationGroupeQuestionnaire(models.Model):
    groupe     = models.ForeignKey(Groupe,on_delete=models.CASCADE)
    questionnaire_groupe   = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)

def get_groupe_QCM(infoGroupe):
    return AssociationGroupeQuestionnaire.objects.filter(groupe=infoGroupe)

