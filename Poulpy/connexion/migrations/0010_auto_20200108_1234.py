# Generated by Django 2.1 on 2020-01-08 12:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('connexion', '0009_auto_20200108_0105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='associationgroupeeleve',
            name='eleve_groupe',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
