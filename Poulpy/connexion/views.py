from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import hashlib

#import des forms
from .forms import *

#import du model
from .models import *

# Create your views here.

#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction basique
#-------------------------------------------------------------------------------------------------------------------------------------------------

def accueil_nc(request):
    return render(request, 'connexion/accueil_nc.html', locals())

def inscrire(request):
    return render(request, 'connexion/inscrire.html',locals())

def plus_information(request):
    return render(request, 'connexion/plus_information.html', locals())

from django.conf import settings
from django.shortcuts import redirect

@login_required
def accueil_eleve(request):
        return render(request, 'eleve/accueil_eleve.html', locals())

@login_required
def accueil_enseignant(request):
        return render(request, 'enseignant/accueil_enseignant.html', locals())

@login_required
def qcm_enseignant(request):
        return render(request, 'enseignant/qcm_enseignant.html', locals())

#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction connexion
#-------------------------------------------------------------------------------------------------------------------------------------------------

from django.contrib.auth import authenticate, login
def connexion(request):
    error = False
    if request.method == "POST":
        form = ConnexionForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)

            if user:
                login(request, user)
                role = getProfilWithUser(user.id)
                if(role[0].roleUt==1):
                    return redirect(reverse(accueil_eleve))
                elif(role[0].roleUt==2):
                    return  redirect(reverse(accueil_enseignant))

            else:
                error = True
    else:
        form = ConnexionForm()

    return render(request, 'connexion/connexion.html', locals())


from django.contrib.auth import logout
from django.shortcuts import redirect
from django.urls import reverse
def deconnexion(request):
    logout(request)
    return redirect(reverse(accueil_nc))




#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction Inscription élève
#-------------------------------------------------------------------------------------------------------------------------------------------------
def inscriptionEleve(request):
    error = 0
    if request.method ==  "POST":
        form = InscriptionFormEleve(request.POST)
        if form.is_valid():
            username        = form.cleaned_data["email"]
            first_name      = form.cleaned_data["first_name"]
            last_name       = form.cleaned_data["last_name"]
            email           = form.cleaned_data["email"]
            password        = form.cleaned_data["password"]
            confirmPwd      = form.cleaned_data["confirmPwd"]
            user = authenticate(username=username, password=password)
            if user :
                erreur = 1
                return render(request, 'connexion/inscription_eleve.html', locals())
            else :
                if(password!=confirmPwd):
                    erreur = 2
                    return render(request, 'connexion/inscription_eleve.html', locals())
                else :
                    user = User.objects.create_user(username,email,password)
                    user.first_name=first_name
                    user.last_name=last_name
                    user.save()
                    profil = Profil(user=user)
                    profil.roleUt=1
                    profil.save()

                    u = authenticate(username=username, password=password)
                    login(request, u)
                    return redirect(reverse(accueil_eleve))
    else :
        form = InscriptionFormEleve()
    return render(request, 'connexion/inscription_eleve.html', locals())

#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction Inscription Enseignant
#-------------------------------------------------------------------------------------------------------------------------------------------------
def inscriptionEnseignant(request):
    error = 0
    if request.method ==  "POST":
        form = InscriptionFormProfesseur(request.POST)
        if form.is_valid():
            username        = form.cleaned_data["email"]
            first_name      = form.cleaned_data["first_name"]
            last_name       = form.cleaned_data["last_name"]
            email           = form.cleaned_data["email"]
            confirmEmail    = form.cleaned_data["confirmEmail"]
            ville           = form.cleaned_data["ville"]
            password        = form.cleaned_data["password"]
            confirmPwd      = form.cleaned_data["confirmPwd"]
            user = authenticate(username=username, password=password)
            if user :
                erreur = 1
                return render(request, 'connexion/inscription_enseignant.html', locals())
            else :
                if(password!=confirmPwd):
                    erreur = 2
                    return render(request, 'connexion/inscription_enseignant.html', locals())
                elif(email!=confirmEmail):
                    erreur = 3
                    return render(request, 'connexion/inscription_enseignant.html', locals())
                else :
                    user = User.objects.create_user(username,email,password)
                    user.first_name=first_name
                    user.last_name=last_name
                    user.save()
                    profil = Profil(user=user)
                    profil.roleUt=2
                    profil.save()
                    u = authenticate(username=username, password=password)
                    login(request, u)
                    return redirect(reverse(accueil_enseignant))
    else :
        form = InscriptionFormProfesseur()
    return render(request, 'connexion/inscription_enseignant.html', locals())

#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction Création QCM
#-------------------------------------------------------------------------------------------------------------------------------------------------
@login_required
def creationQCM(request):
    error = 0
    if request.method == "POST":
        formQuestionnaire = QuestionnaireCreationForm(request.POST)
        if formQuestionnaire.is_valid():
            titreQCM        = formQuestionnaire.cleaned_data["titreQCM"]
            dureeQCM        = formQuestionnaire.cleaned_data['dureeQCM']
            nbMaxQst        = formQuestionnaire.cleaned_data['nbMaxQst']
            dateDebQCM      = formQuestionnaire.cleaned_data['dateDebQCM']
            dateFinQCM      = formQuestionnaire.cleaned_data['dateFinQCM']
            nbEssaiQCM      = formQuestionnaire.cleaned_data['nbEssaiQCM']
            entrainement    = formQuestionnaire.cleaned_data['entrainement']
            questionnaire   = Questionnaire(titreQCM=titreQCM, dureeQCM=dureeQCM,
            nbMaxQst=nbMaxQst, dateDebQCM=dateDebQCM, dateFinQCM=dateFinQCM,
            nbEssaiQCM=nbEssaiQCM, entrainement=entrainement)
            questionnaire.save()
            id_QCM=questionnaire.id
            mail = request.user.get_username()
            user = get_user_by_mail(mail)[0]
            print(user.id)
            print(id_QCM)
            AssocUserQCM    = AssociationCreateurQCM(qcm=questionnaire,createur=user)
            AssocUserQCM.save()

            return redirect('creationQuestion',id_QCM=id_QCM, nbMaxQst=nbMaxQst)
    else :
        formQuestionnaire = QuestionnaireCreationForm()
    return render(request, "enseignant/creationQCM.html", locals())

#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction Création Question
#-------------------------------------------------------------------------------------------------------------------------------------------------
@login_required
def creationQuestion(request, id_QCM, nbMaxQst,):
    error=0
    num_question=1
    num_reponse=1
    if request.method == "POST" :
        formQuestion = QuestionCreationForm(request.POST)
        if formQuestion.is_valid():
            intituleQst     = formQuestion.cleaned_data["intituleQst"]
            nomType         = formQuestion.cleaned_data["nomType"]
            #image           = formQuestion.cleaned_data["image"]
            nbReponse       = formQuestion.cleaned_data["nbReponse"]
            question        = Question(intituleQst=intituleQst,quest=get_QCM_with_ID(id_QCM)[0])
            question.save()
            typeQuestion    = TypeQuestion(question=question, nomType=nomType)
            typeQuestion.save()
            id_question=question.id
            return redirect('creationReponse',id_QCM,nbMaxQst,id_question,nbReponse,num_reponse)
    elif num_question==nbMaxQst+1 :
        return redirect('home_enseignant')
    else :
        id_QCM=id_QCM
        nbMaxQst=nbMaxQst
        formQuestion = QuestionCreationForm()
    return render(request,'enseignant/creationQuestion.html',locals())


@login_required
def creationReponse(request,id_QCM, nbMaxQst,id_question, nombreReponse, num_reponse):
    error=0
    num_reponse=num_reponse
    id_QCM=id_QCM
    nbMaxQst=nbMaxQst
    id_question=id_question
    nombreReponse=nombreReponse
    if request.method == "POST":
        formReponse = ReponseCreationForm(request.POST)
        if formReponse.is_valid():
            pointReponse    = formReponse.cleaned_data["pointReponse"]
            pointMalus      = formReponse.cleaned_data["pointMalus"]
            intituleReponse = formReponse.cleaned_data["intituleReponse"]
            question= get_Question_With_ID(id_question)[0]
            reponse         = Reponse(question=question,intituleReponse=intituleReponse, pointReponse=pointReponse,
            pointMalus=pointMalus)
            reponse.save()

            if(nombreReponse==num_reponse):
                nbMaxQst-=1
                return redirect('creationQuestion', id_QCM, nbMaxQst)
            else :
                num_reponse+=1
                return redirect('creationReponse',id_QCM, nbMaxQst,id_question,nombreReponse,num_reponse)
    else :
        formReponse         = ReponseCreationForm()
    return render(request, 'enseignant/creationReponse.html', locals())
#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction Création Groupe
#-------------------------------------------------------------------------------------------------------------------------------------------------

@login_required
def creationGroupe(request):
    error = 0
    if request.method == "POST":
        formGroupe = GroupeCreationForm(request.POST)
        if formGroupe.is_valid():
            nomGroupe     = formGroupe.cleaned_data['nomGroupe']
            mdp           = formGroupe.cleaned_data['mdp']
            prive         = formGroupe.cleaned_data['prive']
            createur_mail = request.user.get_username()
            createur      = get_user_by_mail(createur_mail)[0]
            groupe        = Groupe(nom_groupe=nomGroupe,createur=createur,prive=prive)
            groupe.save()
            if prive :
                associationPrive =  AssociationGroupeMDP(nom_groupe_asso=groupe,mdp=mdp)
                associationPrive.save()
            return redirect(reverse(consultationGroupes))

    else:
        formGroupe = GroupeCreationForm()
    return render(request, "enseignant/creationGroupe.html", locals())


#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction Consultation Groupes
#-------------------------------------------------------------------------------------------------------------------------------------------------

@login_required
def consultationGroupes(request):
    createur_mail = request.user.get_username()
    createur      = get_user_by_mail(createur_mail)[0]
    listeGroupe   =  get_groupe_Createur(createur)
    return render(request, "enseignant/consultationGroupes.html", locals())


@login_required
def consultationGroupe(request,idG):
    createur_mail = request.user.get_username()
    createur      = get_user_by_mail(createur_mail)[0]
    infoGroupe    =  get_groupe(idG)[0]
    if len(get_association_groupe_mdp(infoGroupe))!=0:
        infoGroupeA   = get_association_groupe_mdp(infoGroupe)[0]
    infoGroupeE   = get_association_groupe_eleve(infoGroupe)
    tailleInfoGroupeE = len(infoGroupeE)
    infoGroupeQCM = get_groupe_QCM(infoGroupe)
    tailleInfoGroupeQCM = len(infoGroupeQCM)
    return render(request, "enseignant/consultationGroupe.html", locals())

@login_required
def consultationGroupeSuppr(request,idG,idEleve):
    infoGroupe    =  get_groupe(idG)[0]
    suppress_association_groupe_eleve(idEleve,infoGroupe)
    return redirect('consultationGroupe',idG)

@login_required
def consulterQCM(request,idG,idQCM):
    return render(request,'enseignant/consulterQCM.html',locals())

@login_required
def supprimerlienQCMGroupe(request,idG,idQCM):
    infoGroupe    =  get_groupe(idG)[0]
    suppress_association_groupe_QCM(idQCM,infoGroupe)
    return redirect('consultationGroupe',idG)

@login_required
def ajouterQCM(request,idG):
    infoGroupe       =  get_groupe(idG)[0]
    formQCM =  LienGroupeQCMForm()
    error=0
    if request.method == "POST":
        formQCM  = LienGroupeQCMForm(request.POST)
        if formQCM.is_valid():
            utilisateur = get_user_by_mail(request.user.get_username())[0]
            asso_qcm_crea = get_questionnaire_createur(utilisateur)
            questionnairesUtilisateur =[]
            for elem in asso_qcm_crea:
                questionnairesUtilisateur.append(elem.qcm)
            questionnaires_association = get_groupe_QCM(infoGroupe)
            res=[]
            for ele in questionnaires_association:
                res.append(ele.questionnaire_groupe)
            qcmId     = formQCM.cleaned_data['qcm']
            qcm        =  get_QCM_with_ID(qcmId)[0]
            print(res)
            if qcm not in res :
                questionnaire  = AssociationGroupeQuestionnaire(groupe=infoGroupe,questionnaire_groupe=qcm)
                questionnaire.save()
                return redirect('consultationGroupe',idG)
            formQCM = LienGroupeQCMForm()
            error=1
    else:
        formQCM = LienGroupeQCMForm()

    return render(request,'enseignant/ajouterQCM.html',locals())

@login_required
def ajouterEleve(request,idG):
    infoGroupe       =  get_groupe(idG)[0]
    error=0
    if request.method == "POST":
        formEleve    = EleveForm(request.POST)
        if formEleve.is_valid():
            eleves = get_all_user()
            eleves_association = get_association_groupe_eleve(infoGroupe)
            mail     = formEleve.cleaned_data['eleve']
            eleve    =  get_user_by_mail(mail)[0]
            eleves_associations=[]
            for el in eleves_association:
                eleves_associations.append(el.eleve_groupe)
            if eleve not in eleves_associations and mail!=request.user.get_username():
                groupe   = AssociationGroupeEleve(nom_groupe=infoGroupe,eleve_groupe=eleve)
                groupe.save()
                return redirect('consultationGroupe',idG)
            formEleve = EleveForm()
            error=1
    else:
        formEleve = EleveForm()
    return render(request,'enseignant/ajouterEleve.html',locals())

#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction Recherche
#-------------------------------------------------------------------------------------------------------------------------------------------------
@login_required
def rechercheEnseignant(request):
    if request.method == "POST":
        formRecherche = RechercheForm(request.POST)
        if formRecherche.is_valid():
            resultat = formRecherche.cleaned_data['recherche']
            groupe = get_groupe_lettre(resultat)
        else:
            groupe = get_groupes()
    else:
        groupe = get_groupes()
        formRecherche = RechercheForm()
    return render(request, "enseignant/rechercheEnseignant.html", locals())

@login_required
def rechercheEleve(request):
    return render(request, "eleve/rechercheEleve.html", locals())
#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction qcm realises et crees
#-------------------------------------------------------------------------------------------------------------------------------------------------
@login_required
def qcmRealisesEnseignant(request):
    return render(request, "enseignant/qcmRealisesEnseignant.html", locals())

@login_required
def qcmCreesEnseignant(request):
    return render(request, "enseignant/qcmCreesEnseignant.html", locals())

#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction qcm realises et crees
#-------------------------------------------------------------------------------------------------------------------------------------------------
@login_required
def consulterProfilEnseignant(request):
    return render(request, "enseignant/consulterProfilEnseignant.html", locals())

#-------------------------------------------------------------------------------------------------------------------------------------------------
# Fonction faire un QCM
#-------------------------------------------------------------------------------------------------------------------------------------------------
@login_required
def faireQCMEnseignant(request):
    return render(request, "enseignant/faireQCMEnseignant.html", locals())
