from django import forms
from .models import User, get_all_user, get_association_groupe_eleve, get_groupe,  get_all_questionnaires, Groupe,get_questionnaire_createur,get_user_by_mail, AssociationCreateurQCM

class ConnexionForm(forms.Form):
    username    = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password    = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
    #widget=forms.PasswordInput Permet de garder le mot de passe secret lors de l'écriture

class InscriptionFormEleve(forms.Form):
    first_name      = forms.CharField(label="Prénom", max_length=25)
    last_name       = forms.CharField(label="Nom de famille", max_length=25)
    email           = forms.EmailField(label="Email", max_length=50)
    numEtudiant     = forms.IntegerField(label="Numéro Étudiant")
    password        = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
    confirmPwd      = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)

class InscriptionFormProfesseur(forms.Form):
    first_name      = forms.CharField(label="Prénom", max_length=25)
    last_name       = forms.CharField(label="Nom de famille", max_length=25)
    email           = forms.EmailField(label="Email", max_length=100)
    confirmEmail    = forms.EmailField(label="Confirmation Email", max_length=100)
    numen           = forms.IntegerField(label="Numéro enseignant (Numen)")
    ville           = forms.CharField(label="Ville", max_length=50)
    password        = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
    confirmPwd      = forms.CharField(label="Confirmation Mot de passe", widget=forms.PasswordInput)


class QuestionnaireCreationForm(forms.Form):
    titreQCM        = forms.CharField(label="Titre du QCM", max_length=100)
    dureeQCM        = forms.DurationField(label="Durée de votre QCM (en minute)")
    nbMaxQst        = forms.DecimalField(label="Nombre de questions maximums pour votre QCM", min_value=1)
    dateDebQCM      = forms.DateField(label="Date de commencement de votre QCM (JJ/MM/AAAA)")
    dateFinQCM      = forms.DateField(label="Date de fin de votre QCM (JJ/MM/AAAA)")
    nbEssaiQCM      = forms.DecimalField(label="Nombre de tentative possible", min_value=1)
    entrainement    = forms.BooleanField(label="QCM d'entrainement ?",required=False)

class QuestionCreationForm(forms.Form):
    intituleQst     = forms.CharField(label="Titre de la question")
    choix           = [("1", "number"), ("2", "text"),("3","radio"),("4","checkbox")]
    nomType         = forms.ChoiceField(choices=choix,label="Type de la question")
    # image           = forms.ImageField()
    choixReponse    = [("1", "1"), ("2", "2"),("3","3"),("4","4"),("5","5")]
    nbReponse       = forms.ChoiceField(choices=choixReponse,label="Nombre de réponse possible")

class ReponseCreationForm(forms.Form):
    intituleReponse = forms.CharField(label="Reponse Possible")
    pointReponse    = forms.IntegerField(label = "Points à obtenir pour cette réponse")
    pointMalus      = forms.IntegerField(label = "Points Malus pour cette réponse")


class GroupeCreationForm(forms.Form):
    nomGroupe       = forms.CharField(label="Nom du groupe ")
    prive           = forms.BooleanField(label="Groupe privé ",required=False)
    mdp             = forms.CharField(widget=forms.PasswordInput,required=False)


class EleveForm(forms.Form):
    eleves = get_all_user()
    choixReponse=[]
    for el in eleves:
        choixReponse.append((el.email,el.email))
    eleve          = forms.ChoiceField(choices=choixReponse,label="Choisir l'élève ")


class RechercheForm(forms.Form):
    recherche      = forms.CharField(required=False)

class LienGroupeQCMForm(forms.Form):
    asso_qcm_crea = get_all_questionnaires()
    choixReponse=[]
    for elem in asso_qcm_crea:
        choixReponse.append(( elem.id , elem.titreQCM))
    qcm = forms.ChoiceField(choices=choixReponse,label="Choisir le QCM ")

