from django.urls import path,re_path
from . import views

urlpatterns=[
    path('', views.accueil_nc, name="accueil"),
    path('plus_d\'information', views.plus_information, name ="info"),
    path('inscription', views.inscrire, name="s'inscrire"),
    path('connexion', views.connexion, name='connexion'),
    path('deconnexion', views.deconnexion, name='deconnexion'),
    path('home_eleve', views.accueil_eleve, name='home_eleve'),
    path('home_enseignant', views.accueil_enseignant, name='home_enseignant'),
    re_path(r'^inscription_eleve$', views.inscriptionEleve, name='inscription_eleve'),
    re_path(r'^inscription_enseignant$', views.inscriptionEnseignant, name='inscription_enseignant'),
    path('enseignant/creation_QCM', views.creationQCM, name='creation_QCM' ),
    path('enseignant/creationGroupe', views.creationGroupe, name='creationGroupe'),
    path('enseignant/creationQuestion/<int:id_QCM>/<int:nbMaxQst>', views.creationQuestion, name='creationQuestion'),
    path('enseignant/creationReponse/<int:id_QCM>/<int:nbMaxQst>/<int:id_question>/<int:nombreReponse>/<int:num_reponse>', views.creationReponse, name='creationReponse'),
    path('enseignant/consultationGroupes', views.consultationGroupes, name='consultationGroupes'),
    path('enseignant/rechercheEnseignant', views.rechercheEnseignant, name='rechercheEnseignant'),
    path('enseignant/qcmRealisesEnseignant', views.qcmRealisesEnseignant, name='qcmRealisesEnseignant'),
    path('enseignant/qcmCreesEnseignant', views.qcmCreesEnseignant, name='qcmCreesEnseignant'),
    path('enseignant/consulterProfilEnseignant', views.consulterProfilEnseignant, name='consulterProfilEnseignant'),
    path('enseignant/faireQCMEnseignant', views.faireQCMEnseignant, name='faireQCMEnseignant'),
    path('eleve/rechercheEleve', views.rechercheEleve, name='rechercheEleve'),
    path('enseignant/consultationGroupe/<int:idG>', views.consultationGroupe, name='consultationGroupe'),
    path('enseignant/consultationGroupeSuppr/<int:idG>/<int:idEleve>', views.consultationGroupeSuppr, name='consultationGroupeSuppr'),
    path('enseignant/ajouterEleve/<int:idG>', views.ajouterEleve, name='ajouterEleve'),
    path('enseignant/consulterQCM/<int:idG>/<int:idQCM>', views.consulterQCM, name='consulterQCM'),
    path('enseignant/ajouterQCM/<int:idG>', views.ajouterQCM, name='ajouterQCM'),
    path('enseignant/supprimerlienQCMGroupe/<int:idG>/<int:idQCM>', views.supprimerlienQCMGroupe, name='supprimerlienQCMGroupe')

]
